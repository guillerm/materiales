# Ejercicios

## Introducción

### <a name="segundos">Días, horas, minutos y segundos</a>

Escribe un programa en Python3 que lea (de teclado) una cantidad de segundos y los convierta en días, horas, minutos y segundos. El programa, al arrancar, mostrará un mensaje en en pantalla:

```
Número de segundos:
```

y esperará que el usuario escriba un número entero, y la tecla ·"Enter". A continuación escribirá:

```
xxx segundos son ddd día(s), hhh hora(s), mmm minuto(s) y sss segundo(s)
```

Por ejemplo, si se introduce el número 90061, el programa escribirá:

```
90061 segundos son 1 día(s), 1 hora(s), 1 minuto(s) y 1 segundo(s)
```

En la cabecera del programa, pon un comentario que indique qué hace el programa, y el nombre de quien lo ha escrito.

* [Solución](intro-segundos/segundos.py)

* [Solución con funciones y tests](intro-segundos-2/segundos.py) (para probar tests)

* [Solución con funciones y tipos](intro-segundos-3/segundos.py) (para probar MyPy)

## Estructuras de control

### <a name="impares">Suma de números impares</a>

Construir un programa que realice la suma de todos los números impares comprendidos entre dos números enteros no negativos introducidos por teclado por el usuario.

Al arrancar, el programa escribirá: `Dame un número entero no negativo: `. Cuando el usuario escriba el número, el programa escribirá: `Dame otro: `. A continuación, el programa mostrará por pantalla la suma de los números impares comprendidos entre esos dos números, incluidos cualquiera de ellos si es un número impar.

Llama al programa `impares.py`.

* **Fecha de entrega:** 12 de octubre de 2022, 23:59
* Entrega en foro de ejercicios
* [Solución](control-impares/impares.py)

### <a name="primos">Cálculo de números primos</a>

* **Fecha de entrega:** 19 de octubre de 2022, 23:59
* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/primos

Construir un programa que solicite un número entero no negativo al usuario, escribiendo el mensaje "Dame un número entero no negativo: ", y que como respuesta escriba en una línea el mensaje "Números primos iguales o menores: ", y a continuaciòn, en la misma línea, la lista de los números primos menores o iguales que él, separados por espacios. Para calcular los números primos, utiliza la definición: "un número entero es primo si sólo es divisible por 1 y por él mismo".

Un ejemplo de ejecución podría ser:

```commandline
Dame un número entero no negativo: 11
Números primos iguales o menores: 1 2 3 5 7 11
```

Otro podría ser:

```commandline
Dame un número entero no negativo: 12
Números primos iguales o menores: 1 2 3 5 7 11
```

Llama al programa `primos.py`

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

## Divide y vencerás

### <a name="triangulo">Triángulo de números</a>

* **Fecha de entrega:** 26 de octubre de 2022, 23:59
* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/triangulo

Escribir un programa que acepte como argumento (en línea de comandos) un número, y muestre un triángulo de números de la forma siguiente: primero una línea con un 1, luego una línea con dos 2, luego una línea con tres 3, y así hasta una línea que tenga número veces número.

Por ejemplo, si el número que se pasó como argumento es 4, de esta forma:

```commandline
python3 triangulo.py 4
```

se mostrará:

```
1
22
333
4444
```

El programa debe llamarse `triangulo.py`, y debe usar el esquema que muestra el fichero `esquema.py` que puedes encontrar en el repositorio de plantilla de esta práctica. Como verás, en ese esquema, todo el código va en la función `main`, y el "programa principal" (el código que va después de `if __name__ ...`) servirá simplemente para llamar a `main`, que hará todo el trabajo, llamando a su vez a las funciones que necesite. Para escribir tu programa te recomendamos que copies el contenido de `esquema.py` en `triangulo.py`, y completes luego `triangulo.py` hasta que funcione como se especifica en este enunciado.

El programa deberá tener, además de `main`, al menos dos funciones:

* `line`, que aceptara como parámetro un número entero, y devolverá una cadena de texto (string) con la línea correspondiente a ese número. Por ejemplo, si el parámetro es el número `3`, devolverá la cadena `333` (sin fin de línea al final).
* `triangle`, que aceptará como parámetro un número entero, y devolverá una cadena de texto (string) con las líneas correspondientes a ese número (incluyendo fines de línea al final de cada cadena). Por ejemplo, si el parámetro es el número `3`, devolverá la cadena `1\n22\n333\n` (ver más adelante el significado de `\n`). Si el parámetro es mayor que 9, levantará la excepción 'ValueError'

La función `main` leerá el valor del argumento pasado en la línea de comandos, y llamará a la función `triangle` con el valor de ese argumento (como entero). Si la función levanta la excepción `ValueError`, volverá a llamar a `triangle` con el valor 9. Luego, mostrará el texto que le haya devuelto `triangle` en pantalla.

Para poder escribir correctamente estas funciones, ten en cuenta lo siguiente:

* El operador `+` se puede utilizar con las cadenas de texto, y el resultado es que concatenará las cadenas a las que se aplique el operador. Por ejemplo, la expresión `"A" + "B"` tiene como resultado la cadena (string) `"AB"`, o la expresión `"ABC" + "DE"` tiene como resultado la cadena `"ABCDE"`.
* El operador `*`, cuando se aplica a una cadena y un número entero, produce como resultado una cadena igual a la origina, repetida ese número de veces. Por ejemplo, `"ABC" * 4` produce como resultado `"ABCABCABCABC"`. 
* El carácter `\n` se interpreta en Python como el fin de línea. Así, la cadena `"ABC\n"` se interpeta como la cadena `ABC` seguida de un fin de línea.
* Al mostrar con `print` una cadena que incluya fines de línea, cada fin de línea hará que en pantalla aparezca una nueva línea. Así, la cadena `print("A\nBB\n")` mostrará en pantalla:

```
A
BB
```

Por otro lado, para leer un argumento de la línea de comandos, puedes utilizar la variable `sys.argv`, que "prerellena" el intérrpete de Python. Por ahora basta con que sepas que el valor primer argumento que recibe un programa puede consultarse como `sys.argv[1]`, y será siempte una cadena (string). Más adelante verás que `sys.argv` es una lista, y podrás entender mejor esta sintaxis. Para poder consultar `sys.argv` hay que importar primero el módulo `sys`, con una línea como la siguiente:

```python
import sys
```

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`. El proceso resumido para hacerlo es el siguiente:

* Cuando hayas creado el repo bifurcado (fork) del repositorio plantilla, consigue su URL HTTPS de clonado, usando el botón "Clone" del repositorio en ETSIT GitLab.
* Utiliza, en tu ordenador, esa url para clonar el repositorio en un directorio local. Si `<url>` es la URL que has obtenido de tu repositorio en GitLab:

```commandline
git clone <url>
```

* Eso te producirá un directorio que tendrá el mismo nombre que el final de la URL del repositorio, en este caso, `triangulo`.

* Abre ese directorio como proyecto en GitLab, y modifica el código de `triangulo.py` hasta que el programa funcione como debe.

* Crea al menos un commit (versión) con PyCharm o con git en línea de comandos. Si lo haces desde la línea de comandos, tendrás que ejecutar (ojo con el punto al final, y con las comillas al principio y al final del comentario que quieras escribir):

```commandline
git add triangulo.py
git commit -m "Comentario de lo que hace el commit" .
```

Los comandos `git` los tendrás que ejecutar desde el directorio donde tienes tu repositiro git local, así que primero tendrás que haberte "cambiado" a él (`cd triangulo` o algo parecido), o ejecutarlo desde el terminal de PyCharm para este proyecto.

* Sube el commit con PyCharm o con git en línea de comando a tu repositirio en ETSIT GitLab. Si lo haces desde la línea de comando:

```commandline
git push
```

Al terminar, comprueba en ETSIT GitLab que el fichero `triangulo.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en ETSIT GitLab como "público" o "interno", para que podamos recoger la práctica.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_triangulo.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_triangulo.py
```